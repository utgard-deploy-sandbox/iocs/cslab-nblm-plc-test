#!/usr/bin/env iocsh.bash

require(s7plc)
require(modbus)
require(calc)

#epicsEnvSet("TOP", "../cwm-cws03_ctrl-plc-01")
epicsEnvSet("TOP", "../pbi_nblm01_ctrl_plc_100")
cd $(TOP)

iocshLoad("pbi_nblm01_ctrl_plc_100.iocsh", "IPADDR=172.30.5.218, RECVTIMEOUT=2000, pbi_nblm01_ctrl_plc_100_VERSION=plcfactory")

